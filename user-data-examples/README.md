# How to use the user data

(for testing purposes only)

- Unpack the data into /tmp/import folder (thus /tmp/import/europdx/... will be created) - this is the folder where the data from pipeline are normally uploaded
- run ~/sandbox-api/import.sh
- the data should appear in sandbox's dataportal
