# How to use a seed sb from this folder

To use a concrete seed db from this folder: copy the respective graph.tgz into ~/data4reset/neo4j-data/graph.tgz - it will be used in ~/sandbox-api/app/reset.sh and import.sh scripts
