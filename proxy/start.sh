#!/bin/sh

# create fqdn.conf
echo "Define FQDN ${HOST}" >/etc/apache2/fqdn.conf
echo "Define EMAILADMIN ${EMAIL_ADMIN_USER}" >> /etc/apache2/fqdn.conf

# set password file
htpasswd -c -b /etc/apache2/.htpasswd administrator ${ADMIN_PASSWORD}

# run Apache
/usr/sbin/apache2ctl -D FOREGROUND
